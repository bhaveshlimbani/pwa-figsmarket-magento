<?php

namespace Aureatelabs\CustomerAddressApi\Plugin;

/**
 * Class BillingAddressPersister
 * @package Aureatelabs\CustomerAddressApi\Plugin
 */

class BillingAddressPersister
{

    /**
     * Logger.
     *
     * @var Logger
     */
    protected $logger;

    /**
     * @param Logger $logger
     *
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }
    
    public function beforeSave(
        \Magento\Quote\Model\Quote\Address\BillingAddressPersister $subject,
        $quote,
        \Magento\Quote\Api\Data\AddressInterface $address,
        $useForShipping = false
    ) {

        $extAttributes = $address->getExtensionAttributes();
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body, true);

        if (!empty($data)) {

            if (array_key_exists("addressInformation", $data)) {

                $customattribute = $data["addressInformation"]["billing_address"]["customAttributes"];

                if (!empty($extAttributes)) {

                    try {

                        foreach ($customattribute as $billingaddressattribute) {

                            if ($billingaddressattribute["attribute_code"] == "block") {

                                $address->setBlock($billingaddressattribute["value"]);
                            }
                            if ($billingaddressattribute["attribute_code"] == "floor") {

                                $address->setFloor($billingaddressattribute["value"]);
                            }
                            if ($billingaddressattribute["attribute_code"] == "avenue") {

                                $address->setAvenue($billingaddressattribute["value"]);
                            }
                        }
                    } catch (\Exception $e) {

                        $this->logger->critical($e->getMessage());
                    }
                }
            }
        }
    }
}
