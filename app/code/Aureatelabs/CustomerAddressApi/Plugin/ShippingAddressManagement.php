<?php


namespace Aureatelabs\CustomerAddressApi\Plugin;

/**
 * Class ShippingAddressManagement
 * @package Aureatelabs\CustomerAddressApi\Plugin
 */

class ShippingAddressManagement
{

    /**
     * Logger.
     *
     * @var Logger
     */
    protected $logger;

    /**
     * @param Logger $logger
     *
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    public function beforeAssign(
        \Magento\Quote\Model\ShippingAddressManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address
    ) {

        $extAttributes = $address->getExtensionAttributes();
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body,true);
       
        if (array_key_exists("customAttributes", $data["addressInformation"]["shipping_address"])) {
            $customattribute =  $data["addressInformation"]["shipping_address"]["customAttributes"];
            if (!empty($extAttributes)) {

                try {
                    foreach($customattribute as $shippingaddressattribute){
                        if($shippingaddressattribute["attribute_code"] == "block"){
                            $address->setBlock($shippingaddressattribute["value"]);
                        }
                        if($shippingaddressattribute["attribute_code"] == "floor"){
                            $address->setFloor($shippingaddressattribute["value"]);
                        }
                        if($shippingaddressattribute["attribute_code"] == "avenue"){
                            $address->setAvenue($shippingaddressattribute["value"]);
                        }
                    }
                    
                    //var_dump($address->getBlock());
                    //die();
                } catch (\Exception $e) {
                    $this->logger->critical($e->getMessage());
                }

            }

        }
    }   
}