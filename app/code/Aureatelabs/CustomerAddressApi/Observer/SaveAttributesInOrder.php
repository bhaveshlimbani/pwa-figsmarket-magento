<?php
namespace Aureatelabs\CustomerAddressApi\Observer;

/**
 * Class SaveAttributesInOrder
 * @package Aureatelabs\CustomerAddressApi\Observer
 */
class SaveAttributesInOrder implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {

        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getData('order');

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getData('quote');

        $shippingAddressData = $quote->getShippingAddress()->getData();

        if (isset($shippingAddressData['block'])) {

            $order->getShippingAddress()->setBlock($shippingAddressData['block']);

        }
        if (isset($shippingAddressData['floor'])) {

            $order->getShippingAddress()->setFloor($shippingAddressData['floor']);

        }
        if (isset($shippingAddressData['avenue'])) {

            $order->getShippingAddress()->setAvenue($shippingAddressData['avenue']);

        }

       if ($quote->getBillingAddress()) {

            $billingAddressData = $quote->getBillingAddress()->getData();

            if (isset($billingAddressData['block'])) {

                $order->getBillingAddress()->setBlock($billingAddressData['block']);

            }
            if (isset($billingAddressData['floor'])) {

                $order->getBillingAddress()->setFloor($billingAddressData['floor']);

            }
            if (isset($shippingAddressData['avenue'])) {

                $order->getBillingAddress()->setAvenue($billingAddressData['avenue']);

            }

        }

        return $this;
    }
}