<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\CustomerApi\Model;

use Aureatelabs\CustomerApi\Api\CustomersRepositoryInterface;
use Aureatelabs\CustomerApi\Api\Data\CustomersInterfaceFactory;
use Aureatelabs\CustomerApi\Api\Data\CustomersSearchResultsInterfaceFactory;
use Aureatelabs\CustomerApi\Model\ResourceModel\Customers as ResourceCustomers;
use Aureatelabs\CustomerApi\Model\ResourceModel\Customers\CollectionFactory as CustomersCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class CustomersRepository implements CustomersRepositoryInterface
{

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    private $collectionProcessor;

    protected $customersFactory;

    protected $searchResultsFactory;

    private $storeManager;

    protected $resource;

    protected $customersCollectionFactory;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    protected $dataCustomersFactory;


    /**
     * @param ResourceCustomers $resource
     * @param CustomersFactory $customersFactory
     * @param CustomersInterfaceFactory $dataCustomersFactory
     * @param CustomersCollectionFactory $customersCollectionFactory
     * @param CustomersSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceCustomers $resource,
        CustomersFactory $customersFactory,
        CustomersInterfaceFactory $dataCustomersFactory,
        CustomersCollectionFactory $customersCollectionFactory,
        CustomersSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->customersFactory = $customersFactory;
        $this->customersCollectionFactory = $customersCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataCustomersFactory = $dataCustomersFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Aureatelabs\CustomerApi\Api\Data\CustomersInterface $customers
    ) {
        /* if (empty($customers->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $customers->setStoreId($storeId);
        } */
        
        $customersData = $this->extensibleDataObjectConverter->toNestedArray(
            $customers,
            [],
            \Aureatelabs\CustomerApi\Api\Data\CustomersInterface::class
        );
        
        $customersModel = $this->customersFactory->create()->setData($customersData);
        
        try {
            $this->resource->save($customersModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the customers: %1',
                $exception->getMessage()
            ));
        }
        return $customersModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($customersId)
    {
        $customers = $this->customersFactory->create();
        $this->resource->load($customers, $customersId);
        if (!$customers->getId()) {
            throw new NoSuchEntityException(__('Customers with id "%1" does not exist.', $customersId));
        }
        return $customers->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->customersCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Aureatelabs\CustomerApi\Api\Data\CustomersInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Aureatelabs\CustomerApi\Api\Data\CustomersInterface $customers
    ) {
        try {
            $customersModel = $this->customersFactory->create();
            $this->resource->load($customersModel, $customers->getCustomersId());
            $this->resource->delete($customersModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Customers: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($customersId)
    {
        return $this->delete($this->get($customersId));
    }
}

