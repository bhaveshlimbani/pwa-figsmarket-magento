<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\CustomerApi\Model\Customers;

use Aureatelabs\CustomerApi\Model\ResourceModel\Customers\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $loadedData;
    protected $collection;
    protected $_request;
    protected $dataPersistor;


    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = [],
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->_request = $request;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
        }
        $data = $this->dataPersistor->get('aureatelabs_customerapi_customers');
        
        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear('aureatelabs_customerapi_customers');
        }
        
        return $this->loadedData;
    }

    public function getMeta()
    {
        $meta = parent::getMeta();
       
        $parameterarray = $this->_request->getParams('customers_id');        
        if(isset($parameterarray['customers_id'])){

           $meta['general']['children']['token']['arguments']['data']['config']['disabled'] = 1;
           $meta['general']['children']['brand']['arguments']['data']['config']['disabled'] = 1;
           $meta['general']['children']['number']['arguments']['data']['config']['disabled'] = 1;

        }
        return $meta;
    }
}

