<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Banner\Model\Indexer\Action;

use Aureatelabs\Banner\Model\ResourceModel\Banner as BannerResource;
use Magento\Framework\App\Area;
use Magento\Framework\App\AreaList;

/**
 * Class Banner
 * @package Aureatelabs\Banner\Model\Indexer\Action
 */
class Banner
{
    /**
     * @var AreaList
     */
    private $areaList;

    /**
     * @var BannerResource
     */
    private $resourceModel;

    /**
     * @var \Aureatelabs\Banner\Model\ImageUploader
     */
    protected $imageUploader;

    /**
     * Banner constructor.
     *
     * @param AreaList $areaList
     * @param BannerResource $bannerResource
     * @param \Aureatelabs\Banner\Model\ImageUploader $imageUploader
     * @param \Magento\Framework\Serialize\Serializer\Json $json
     */
    public function __construct(
        AreaList $areaList,
        BannerResource $bannerResource,
        \Aureatelabs\Banner\Model\ImageUploader $imageUploader
    ) {
        $this->areaList = $areaList;
        $this->resourceModel = $bannerResource;
        $this->imageUploader = $imageUploader;
    }

    /**
     * @param int $storeId
     * @param array $bannerIds
     *
     * @return \Traversable
     * @throws \Exception
     */
    public function rebuild($storeId = 1, array $bannerIds = [])
    {
        $this->areaList->getArea(Area::AREA_FRONTEND)->load(Area::PART_DESIGN);
        $banners = $this->resourceModel->loadBanners($storeId, $bannerIds);
        foreach ($banners as $banner) {

            $lastBannerId = $banner['entity_id'];
            $banner['id'] = $banner['entity_id'];

            $banner['image'] = $this->imageUploader->getFileUrl($banner['image']);
            $banner['mobile_image'] = $this->imageUploader->getFileUrl($banner['mobile_image']);

            yield $lastBannerId => $banner;
        }
    }
}
